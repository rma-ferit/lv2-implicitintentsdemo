package brunozoric.ferit.hr.implicitintentsdemo

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_implicit.*

class ImplicitActivity : AppCompatActivity() {

    private val TAG : String = "Bruno"
    private val COMPONENT_NOT_FOUND : String = "No component can handle the request"
    private val EMERGENCY_NUBER: String = "911"
    private val HTTP_PREFIX: String = "http://"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_implicit)
        setUpUi()
    }

    private fun setUpUi() {
        browseAction.setOnClickListener{v -> onClick(v.id)}
        dialAction.setOnClickListener{v -> onClick(v.id)}
        shareAction.setOnClickListener{v -> onClick(v.id)}
    }

    private fun onClick(viewId: Int) {
        when(viewId){
            R.id.browseAction -> { browse(etWebAddressInput.text.toString()) }
            // Dodati dial
            // Dodati share
        }
    }

    private fun browse(text: String) {

        var url: String = text.toLowerCase()
        if(url.startsWith(HTTP_PREFIX) == false) url = HTTP_PREFIX + url
        val browseIntent = Intent()
        val uri = Uri.parse(url)
        browseIntent.setData(uri)
        browseIntent.setAction(Intent.ACTION_VIEW)
        startIfPossible(browseIntent)
    }

    private fun startIfPossible(browseIntent: Intent) {
        if(canBeCalled(browseIntent)) startActivity(browseIntent)
        else Log.d(TAG, COMPONENT_NOT_FOUND)
    }

    private fun canBeCalled(browseIntent: Intent): Boolean {
        return browseIntent.resolveActivity(packageManager) != null
    }
}
